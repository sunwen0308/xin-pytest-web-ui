# -*- coding: utf-8 -*-
from selenium.webdriver.common.by import By


class CommodityListPage:
    """一级模块——商品列表模块"""
    #商品模块元素
    commodity_loc = (By.XPATH, '//*[@id="app"]/div/div[1]/div/ul/div/li[1]/div')
    #商品列表元素
    commodity_list_loc = (By.XPATH, '//*[@id="app"]/div/div[1]/div/ul/div/li[1]/ul/a[1]/li/span')
    """------------------筛选搜索------------------------"""
    #商品名称输入搜索元素
    enter_search_loc = (By.XPATH, '//*[@id="app"]/div/div[2]/section/div/div[1]/div/div[2]/form/div[1]/div/div/input')
    #商品货号元素
    product_code_loc = (By.XPATH, '//*[@id="app"]/div/div[2]/section/div/div[1]/div/div[2]/form/div[2]/div/div/input')
    #商品分类元素
    commodity_class_loc = (By.XPATH, '//*[@id="app"]/div/div[2]/section/div/div[1]/div/div[2]/form/div[3]/div/span')
    #商品品牌元素
    commodity_brand_loc = (By.XPATH, '//*[@id="app"]/div/div[2]/section/div/div[1]/div/div[2]/form/div[4]/div/div/div[1]/input')
    #商品品牌苹果品牌元素
    Apple_brand_loc = (By.XPATH, '//span[text()="苹果"]')
    #上架状态拉下元素
    shelf_status_loc = (By.XPATH, '//*[@id="app"]/div/div[2]/section/div/div[1]/div/div[2]/form/div[5]/div/div/div[1]')
    #上架状态元素
    up_shelf_loc = (By.XPATH, '//span[text()="上架"]')
    #下架状态元素
    down_shelf_loc = (By.XPATH, '//span[text()="下架"]')
    #审核状态元素
    audit_status_loc = (By.XPATH, '//*[@id="app"]/div/div[2]/section/div/div[1]/div/div[2]/form/div[6]/div/div/div[1]')
    #重置元素
    reset_loc = (By.XPATH, '//*[@id="app"]/div/div[2]/section/div/div[1]/div/div[1]/button[2]')
    #查询结果元素
    query_results_loc = (By.XPATH, '//*[@id="app"]/div/div[2]/section/div/div[1]/div/div[1]/button[1]')
    #点击确定元素
    submit_yes_loc = (By.XPATH, '/html/body/div[2]/div/div[3]/button[2]')

    """------------------数据列表------------------------"""
    #数据列表的上下架按钮元素
    data_updownshelf_loc = (By.XPATH, '//*[@id="app"]/div/div[2]/section/div/div[3]/div/div[3]/table/tbody/tr/td[6]/div/p[1]/div/span')
    #数据列表的删除按钮元素
    delete_loc = (By.XPATH, '//*[@id="app"]/div/div[2]/section/div/div[3]/div/div[3]/table/tbody/tr/td[11]/div/p[2]/button[2]')
    """断言元素"""
    #商品名称和商品货号断言元素
    enter_searchassert_loc = (By.XPATH, '//*[@id="app"]/div/div[2]/section/div/div[3]/div/div[3]/table/tbody/tr/td[4]/div/p[1]')
    # #商品品牌断言元素
    # enter_searchassert_loc = (By.XPATH, '//*[@id="app"]/div/div[2]/section/div/div[3]/div/div[3]/table/tbody/tr/td[4]/div/p[1]')
    #商品上下架断言元素
    down_shelfassert_loc = (By.XPATH, '//div[class="el-switch is-checked" and aria-checked="true"]')
    """------------------数据列表------------------------"""
