# -*- coding: utf-8 -*-
from selenium.webdriver.common.by import By
from base.base import BasePage
import allure
import pytest
# from PageObjects.CallCenter.callCenter_page import CallCenterPage
from business.commodity_list.commodity_list_bs import CommodityListBusiness as CLB
from time import sleep
from common.read_yaml import ReadYaml
data = ReadYaml("data.yml").get_yaml_data()#读取数据
from business.commodity_list.commodity_list_assert import CommodityListAssert as CLA
from pagelocators.commodity_locs.commodity_list_page import CommodityListPage as CLP

# @pytest.mark.usefixtures("_driver")
# @pytest.mark.data_center
# @pytest.mark.skip
class TestDataCenter():

    #数据中心
    @pytest.mark.test
    # @pytest.mark.usefixtures("close")
    # @pytest.mark.usefixtures("_driver")
    @allure.feature('测试商品模块')
    @allure.story('测试商品列表模块')
    @allure.step('步骤：测试商品名称搜索功能')
    @allure.title("测试商品名称搜索功能搜索正常")
    @pytest.mark.run(order=1)
    def test_enter_search(self,CLB,driver1):
        CLB[1].enter_search(text='HLA海澜之家简约动物印花短袖T恤')
        assert CLA(CLB[0]).enter_search_assert(text='HLA海澜之家简约动物印花短袖T恤')



    @allure.feature('测试商品模块')
    @allure.story('测试商品列表模块')
    @allure.step('步骤：测试商品名称搜索功能')
    @allure.title("测试商品货号搜索功能搜索正常")
    @pytest.mark.run(order=2)
    def test_product_code(self, CLB, driver1):
        CLB[1].product_code()
        assert CLA(CLB[0]).product_code_assert(text='货号：6946605')

    @allure.feature('测试商品模块')
    @allure.story('测试商品列表模块')
    @allure.step('步骤：测试商品品牌搜索功能')
    @allure.title("测试商品品牌搜索功能搜索正常")
    @pytest.mark.run(order=3)
    def test_commodity_brand(self,CLB,driver1):
        CLB[1].commodity_brand()
        assert CLA(CLB[0]).commodity_brand_assert(text='iPhone 8 Plus')

    @allure.feature('测试商品模块')
    @allure.story('测试商品列表模块')
    @allure.step('步骤：测试商品上下架搜索功能')
    @allure.title("测试商品上下架搜索功能搜索正常")
    @pytest.mark.run(order=4)
    def test_shelf_status(self,CLB,driver1):
        CLB[1].shelf_status()
        assert CLA(CLB[0]).down_shelf_assert(text='iPhone 8 Plus')



if __name__ == '__main__':
    pytest.main(['-q', r'D:\daima\pytest-web-ui-automation-huice\tests\test_commodity_list\test_cmmodity_list.py'])